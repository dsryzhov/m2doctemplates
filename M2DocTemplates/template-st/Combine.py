from docxcompose.composer import Composer
from docx import Document

path = "./"

master = Document("MasterTemplate.docx")
composer = Composer(master)

worksDoc = Document(path+"WorkBreakdownTemplate.docx")
composer.append(worksDoc)

requirementsDoc = Document(path+"RequirementsTemplate.docx")
composer.append(requirementsDoc)

contextDoc = Document(path+"ContextTemplate.docx")
composer.append(contextDoc)

usecaseAndScenariosDoc = Document(path+"ScenariosTemplate.docx")
composer.append(usecaseAndScenariosDoc)

funcDecompDoc = Document(path+"FunctionalDecomposition.docx")
composer.append(funcDecompDoc)

componentsDoc = Document(path+"ComponentsTemplate.docx")
composer.append(componentsDoc)

interfacesDoc = Document(path+"InterfacesTemplate.docx")
composer.append(interfacesDoc)

logicalDataDoc = Document(path+"LogicalDataTemplate.docx")
composer.append(logicalDataDoc)

endDoc = Document(path+"EndTemplate.docx")
composer.append(endDoc)

composer.save("LogicalArchitectureTemplate.docx")