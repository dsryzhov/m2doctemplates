from docxcompose.composer import Composer
from docx import Document

path = "c:/Repos/tool/M2DocTemplatesRepo/M2DocTemplates/template-st/"


contentTemplate = Document(path+"ContentTemplate.docx")

contentComposer = Composer(contentTemplate)

ContentSectionsTemplate = Document("SectionsTemplate.docx")
contentComposer.append(ContentSectionsTemplate)

contentComposer.save("../output/template/ContentSelectedTemplate.docx")